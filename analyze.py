"""
loads and analyzes MI (per input) of emotional classification data
"""
from __future__ import division
import os.path
import doctest

import numpy as np
import pandas as pd


def MI(probXY):
    """ computes MI of a vector

    H(x) = E[log P(x, y) / (P(x) P(y))]

    arg:
        probXY = (numX, numY) vector, distribution

    returns:
        MI = scalar, mutual info (in bits)

    >>> probXY = np.ones((2, 4)) / 8
    >>> MI(probXY)
    0.0

    >>> probXY = np.hstack((probXY, np.zeros((2, 4))))
    >>> MI(probXY)
    0.0

    >>> probXY = np.eye(8) / 8
    >>> MI(probXY)
    3.0
    """

    assert probXY.ndim is 2, 'probX must be numpy array of dim 2'
    assert abs(sum(sum(probXY)) - 1) < 1e-6, 'probX must be normed'

    probX = probXY.sum(axis=1)
    probY = probXY.sum(axis=0)

    probX, probY = np.meshgrid(probX, probY)

    def mk_vector(x):
        """ makes a vector out of an array
        """
        return np.reshape(x, np.prod(x.shape))

    # compute MI
    MI = sum([xy * np.log(xy / (x * y))
              for (x, y, xy) in zip(mk_vector(probX),
                                    mk_vector(probY),
                                    mk_vector(probXY.T))
              if all([x, y, xy])]) / np.log(2)

    return MI


def H(probX):
    """ computes entropy of a vector

    H(x) = E[-log P(x)]

    arg:
        probX = (numX) vector, distribution

    returns:
        Hx = scalar, entropy (in bits)

    >>> probX = np.ones(2) * .5
    >>> H(probX)
    1.0

    >>> probX = np.hstack((np.ones(4) / 4, np.zeros(3)))
    >>> H(probX)
    2.0
    """

    assert probX.ndim is 1, 'probX must be numpy array of dim 2'
    assert abs(sum(probX) - 1) < 1e-6, 'probX must be normed'

    # compute Hx
    Hx = sum([-p * np.log(p) for p in probX if p]) / np.log(2)

    return Hx


def get_MI_per_x(probXY):
    """ computes the mutual information per input X (into a DMC X -> Y)

    MI_per_x(x=i) = H(Y) + \sum_y \log P_{y|x}(y|i)

    arg:
        probXY = (numX, numY) array, joint distribution

    returns:
        MI_per_x = (numX) array, MI info per input (not necessarily positive,
                    see ex below)

    dummy example, 3 states are perfectly classified, the 4th is uni guess
    >>> numX = 4
    >>> probXY = np.eye(numX)
    >>> probXY[-1, :] = np.ones(numX) / numX
    >>> probXY = probXY / sum(sum(probXY))

    >>> get_MI_per_x(probXY)
    array([ 1.82319241,  1.82319241,  1.82319241, -0.17680759])

    NOTE: last value is negative as Y received when X is last state actually
    increase (on average) the entropy in X.
    """

    assert probXY.ndim is 2, 'probXY must be numpy array of dim 2'
    assert abs(sum(sum(probXY)) - 1) < 1e-6, 'probXY must be normed'

    probX = probXY.sum(axis=1)
    probY = probXY.sum(axis=0)
    probYgivenX = probXY / probX[:, np.newaxis]

    MI_per_x = H(probY) - np.apply_along_axis(H, 1, probYgivenX)

    # sanity check
    MI_XY = MI(probXY)
    assert abs(np.dot(MI_per_x, probX) - MI_XY) < 1e-6, \
        'MI (traditional) diagrees'

    return MI_per_x

if __name__ == '__main__':
    # run tests in doc
    doctest.testmod()

    # params
    data_folder = u'/home/matt/Dropbox/IAS_emoClassPerfPerCulture/data'
    filename = 'FaceFreeLabel_Coding_BSPIRAL_020816.xls'
    idx_list = ['target emotion', 'basic category']

    # read / groom data (make lower case header and data)
    df = pd.read_excel(os.path.join(data_folder, filename))
    df.columns = [x.lower() for x in df.columns]
    for col in idx_list:
        # note that this removes '999' entries AND makes strings lowercase
        df[col] = df[col].str.lower()

    # count by idx_list pairs
    emo_dmc_cnt = df.groupby(idx_list).size()

    # split multi 'basic categories' among their target categories
    for idx in emo_dmc_cnt.index:
        if ';' not in idx[1]:
            # not a multi 'basic category'
            continue

        # divide number of events among 'basic category' estimates
        estimate_list = idx[1].split('; ')
        eventsPerEst = emo_dmc_cnt[idx] / len(idx_list)

        for est in estimate_list:
            emo_dmc_cnt[idx[0], est] = \
                emo_dmc_cnt.get((idx[0], est), default=0) + \
                eventsPerEst

        # rm old, multi entry
        del emo_dmc_cnt[idx]

    # build joint distribution (note: each row is a target, each col is an est)
    emo_idx_map = {emo: idx for idx, emo
                   in enumerate(emo_dmc_cnt.index.levels[0])}
    numX = len(emo_idx_map)
    probXY = np.zeros((numX, numX))
    for idx, val in emo_dmc_cnt.iteritems():
        probXY[emo_idx_map[idx[0]],
               emo_idx_map[idx[1]]] = val
    probXY = probXY / sum(sum(probXY))

    # get MI_per_emo and graph
    MI_per_emo = get_MI_per_x(probXY)
